mysql -u root -p

INSERT INTO artists (name) 
VALUES ("Taylor Swift"),("Lady Gaga"),("Justin Bieber"),("Ariana Grande"),("Bruno Mars");

INSERT INTO albums (album_title, date_released, artist_id) 
VALUES ("Fearless", "2008", 3),("Red", "2012", 3),("A Star Is Born", "2018", 4),("Born This Way", "2011", 4),("Purpose", "2015", 5),("Believe", "2012", 5),("Dangerous Woman", "2016", 6),("Thank U, Next", "2019", 6),("24K Magic", "2016", 7),("Earth to Mars", "2011", 7);

INSERT INTO songs (song_name, length, genre, album_id) 
VALUES ("Fearless", 246, "Pop rock", 3),("Love Story", 213, "Country pop", 3),("State of Grace", 273, "Rock, alternative rock, arena rock", 4),("Red", 204, "Country", 4),("Black Eyes", 181, "Rock and roll", 5),("Shallow", 201, "Country, rock, folk rock", 5),("Born This Way", 252, "Electropop", 6),("Sorry", 192, "Dancehall-poptropical housemoombahton", 7),("Boyfriend", 251, "Pop", 8),("Into You", 242, "EDM house", 9),("Thank U Next", 196, "Pop, R&B", 10),("24K Magic", 207, "Funk, disco, R&B", 11),("Lost", 192, "Pop", 12);

-- a
SELECT name FROM artists WHERE name LIKE '%d%';

-- b
SELECT song_name, length FROM songs WHERE length < 230;

-- c
SELECT album_title, song_name,length FROM albums
JOIN songs ON albums.id = songs.album_id;

-- d
SELECT album_title FROM artists
JOIN albums ON artists.id = albums.artist_id
WHERE album_title LIKE '%a%';

-- e
SELECT album_title FROM albums ORDER BY album_title DESC LIMIT 4;


-- f
SELECT album_title, song_name,length FROM albums
JOIN songs ON albums.id = songs.album_id
ORDER BY album_title DESC;
